const express = require('express')
const Handlebars = require('handlebars') //Шаблонизатор
const exphbs = require('express-handlebars') //Шаблонизатор
const session = require('express-session') // Поддержка сессий
const MongoStore = require('connect-mongodb-session')(session) //Хранение сессий в БД
const { allowInsecurePrototypeAccess } = require('@handlebars/allow-prototype-access') //Безопасность шаблонизатора (вопрос прототипирования)
const homeRoutes = require('./routes/home')
const addRoutes = require('./routes/add')
const CoursesRoutes = require('./routes/courses')
const cardRoutes = require('./routes/card')
const ordersRoutes = require('./routes/orders')
const authRotes = require('./routes/auth')
const mongoose = require('mongoose')
const User = require('./models/user')
const varMiddleWare = require('./middleware/variables')
const userMiddleWare = require('./middleware/user')
const MONGODB_URI = `mongodb+srv://user_not_admin:O6RT1o8D6xGLT2of@cluster0-mldxs.mongodb.net/shop?retryWrites=true&w=majority`
const app = express()
const store = new MongoStore({
    collection: 'sessions',
    uri: MONGODB_URI

})

const PORT = process.env.PORT || 3000
const path = require('path')

app.engine('.hbs', exphbs({
    handlebars: allowInsecurePrototypeAccess(Handlebars),
    defaultLayout: 'main',
    extname: '.hbs'
}));

app.set('view engine', '.hbs');
app.set('views', 'views')

app.use(express.static(path.join(__dirname, 'public')))
app.use(express.urlencoded({ extended: true }))

app.use(session({
    secret: 'some secret value',
    resave: false,
    saveUninitialized: false,
    store
}))
app.use(varMiddleWare)
app.use(userMiddleWare)

app.use('/auth', authRotes)
app.use('/card', cardRoutes)
app.use('/', homeRoutes)
app.use('/add', addRoutes)
app.use('/courses', CoursesRoutes)
app.use('/orders', ordersRoutes)

const start = async() => {
    try {
        await mongoose.connect(MONGODB_URI, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        })

        app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`)
        })
    } catch (e) {
        console.log(e);
    }
}

start()