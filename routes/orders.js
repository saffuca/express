const { Router } = require('express')
const router = Router()
const Order = require('../models/order')
const auth = require('../middleware/auth')

router.get('/', auth, async(req, res) => {
    try {
        const orders = await Order.find({ 'user.userId': req.user._id })
            .populate('user.userId')
            .sort({ date: 'desc' })

        res.render('orders', {
            isOrder: true,
            title: 'Заказы',
            orders: orders.map(o => {
                return {
                    ...o._doc,
                    price: o.courses.reduce((total, с) => {
                        return total += с.count * с.course.price
                    }, 0)
                }
            })
        })
    } catch (error) {
        console.log(error)
    }
})

router.post('/', auth, async(req, res) => {
    try {
        const user = await req.user
            .populate('cart.items.courseId')
            .execPopulate()

        const courses = user.cart.items.map(item => {
            return {
                count: item.count,
                course: {...item.courseId._doc }
            }
        })

        const order = new Order({
            user: {
                name: req.user.name,
                userId: req.user
            },
            courses
        })
        await order.save()
        await req.user.clearCart()
        res.redirect('/orders')
    } catch (error) {
        console.log(error)
    }


})

module.exports = router