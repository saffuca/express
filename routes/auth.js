const { Router } = require('express')
const router = Router()
const User = require('../models/user')

router.get('/login', async(req, res) => {
    res.render('../views/auth/login.hbs', {
        title: 'Авторизация',
        isLogin: true
    })
})

router.get('/logout', async(req, res) => {
    req.session.destroy(() => {
        res.redirect('/auth/login#login')
    })
})

router.post('/login', async(req, res) => {
    const user = await User.findById('5eafe9334c89be3818ad3762')
    req.session.user = user
    req.session.isAuthenticated = true
    req.session.save(err => {
        if (err) {
            throw error
        }
        res.redirect('/')
    })
})

module.exports = router